# Boot Camel
Simple Spring-Boot Apache Camel project for some hands-on expirience on spring boot camel integration.

To run the this project use `./gradlew bootRun` or `gradlew.bat bootRun` .

Some interesting URLs:

* [Camel endpoint - Hello World](http://localhost:8080/camel/helloCamel)
* [Camel endpoint - list all messages](http://localhost:8080/camel/messages)
* [Camel endpoint - add a new message](http://localhost:8080/camel/createMessage)
* [Hawt.io monitoring console](http://localhost:8080/hawtio/) if you're using the hawtio branch.