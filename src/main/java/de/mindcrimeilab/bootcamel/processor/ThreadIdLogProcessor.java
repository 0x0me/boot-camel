package de.mindcrimeilab.bootcamel.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Small processor just forwarding the message without any modifcation. The
 * only purpose is to print out the thread id at runtime to verify that the
 * route is executed in a certain thread.
 */
public class ThreadIdLogProcessor implements Processor {
    /** log instance */
    Logger log = LoggerFactory.getLogger(ThreadIdLogProcessor.class.getName());

    @Override
    public void process(Exchange exchange) throws Exception {
        // print out name of calling route and id of current thread
        log.info("[" + exchange.getFromRouteId() + "] - Thread is [" + Thread.currentThread().getId() + "]");
    }
}
