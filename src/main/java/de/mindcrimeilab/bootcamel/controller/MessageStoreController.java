package de.mindcrimeilab.bootcamel.controller;

import de.mindcrimeilab.bootcamel.db.Message;
import de.mindcrimeilab.bootcamel.db.MessageReferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Implements a simple REST interface to the data layer.
 *
 */
@RestController
@RequestMapping("/services/messages")
public class MessageStoreController {
    @Autowired
    private MessageReferenceRepository repository;

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Message> messages() {
        return  repository.findAll();
    }
    @RequestMapping(method = RequestMethod.GET, value = "{id}")
    public Message message(@PathVariable Long id) {
        return repository.findOne(id);
    }

}
