package de.mindcrimeilab.bootcamel;

import de.mindcrimeilab.bootcamel.db.Message;
import de.mindcrimeilab.bootcamel.db.MessageReferenceRepository;
import de.mindcrimeilab.bootcamel.processor.ThreadIdLogProcessor;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultMessage;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Simple Route Builder defines all known routes
 */
@Component
public class SimpleRouter extends RouteBuilder {

    /** imap users username */
    @Value("${imap.username}")
    private String username;

    /** imap users password */
    @Value("${imap.password}")
    private String password;

    /** adress of the imap server */
    @Value("${imap.address}")
    private String imapServer;

    @Autowired
    MessageReferenceRepository repository;

    /**
     * Set up routes.
     * @throws Exception
     */
    @Override
    public void configure() throws Exception {
        /*
         * Simple route from the camel introductory samples
         */
        // Access us using http://localhost:8080/camel/hello
        from("servlet:///helloCamel").transform().constant("Hello from Camel!").setId("HelloCamel");


        /*
         * route creates a {@code Message} and send it to asynchronous endpoint ( processing will
         * take place in a different thread) returning yielding always "Ok"
         */
        from("servlet:///createMessage")
                .process(new ThreadIdLogProcessor())
                .to("seda:in")
                .transform(constant("Ok"))
                .setId("CreateMessage");

        /*
         * picks up a message from the seda endpoint persisting it to the database
         * returning the id of the new created message and sending it to the next route
         * directly (processing will take place in the same thread)
         */
        from("seda:in")
                .process(new ThreadIdLogProcessor())
                .log(LoggingLevel.INFO, "Thread is [" + Thread.currentThread().getId() + "]")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        DefaultMessage msg = new DefaultMessage();
                        msg.setBody(createMessage(), Message.class);
                        exchange.setOut(msg);
                    }
                })
                .log(LoggingLevel.INFO, "Before save: ${body}")
                .transform()
                .body(Message.class)
                .beanRef("messageReferenceRepository", "save")
                .log(LoggingLevel.INFO, "After save: ${body}")
                .transform()
                .simple("${body?.id}")
                .to("direct:messages")
                .setId("PersistMessage");

        /*
         * log message (id)
         */
        from("direct:messages")
                .process(new ThreadIdLogProcessor())
                .to("log:out_direct_messages")
                .setId("LogMessage");

        /*
         * implement a partial REST service using data format conversion to JSON and
         * returning the result
         */
        from("servlet:///messages")
                .beanRef("messageReferenceRepository", "findAll()")
                .marshal()
                .json(JsonLibrary.Jackson)
                .setId("RestService");



        // Trigger run right after startup. No Servlet request required.
        //from("timer://foo?fixedRate=true&period=10s").log("Camel timer triggered.");


        from("timer://trigger?fixedRate=true&period=10s")
                .autoStartup(false)
                .to("log:intermediate")
                .transform().simple("${ref:myBean}")
                .choice()
                .when(body().isEqualTo("13")).to("log:out_13").endChoice()
                .otherwise().to("log:out_otherwise").endChoice()
                .end()
                .routeId("TriggerDummyRoute");

        String imapUri = String.format("imaps://%s?username=%s&password=%s"
                + "&delete=false&unseen=false&consumer.delay=60000", imapServer, username, password);

        from(imapUri)
                .autoStartup(false)
                .to("log:newmail")
                .routeId("IncomingMails");
    }


    @Bean
    Message myBean() {
        return createMessage();

    }

    private Message createMessage() {
        Message m = new Message(String.valueOf(System.currentTimeMillis()), "some message");
        return m;
    }

}
