package de.mindcrimeilab.bootcamel.db;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Domain Object
 *
 * Dummy domain object.
 */
@Entity
public class Message implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false)
    private String incomingReference;

    @Column(nullable = false)
    private String outgoingReference;

    Message() {
        // jpa requires non argument constructor
    }

    public Message(String incomingReference, String outgoingReference) {
        this.incomingReference = incomingReference;
        this.outgoingReference = outgoingReference;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIncomingReference() {
        return incomingReference;
    }

    public void setIncomingReference(String incomingReference) {
        this.incomingReference = incomingReference;
    }

    public String getOutgoingReference() {
        return outgoingReference;
    }

    public void setOutgoingReference(String outgoingReference) {
        this.outgoingReference = outgoingReference;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", incomingReference='" + incomingReference + '\'' +
                ", outgoingReference='" + outgoingReference + '\'' +
                '}';
    }
}
