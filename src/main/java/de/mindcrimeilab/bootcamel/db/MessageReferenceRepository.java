package de.mindcrimeilab.bootcamel.db;

import org.apache.camel.Handler;
import org.springframework.data.repository.CrudRepository;

/**
 * Data repository definition to support CRUD operations on the database layer.
 * The {@code CrudRepository} type specification defines the payload / data
 * object ({@code Message}) as well as the primary key class ({@Long}).
 */
public interface MessageReferenceRepository extends CrudRepository<Message, Long> {
    @Handler
    Iterable<Message> findAll();
}
